#!/usr/bin/env bash

docker run -ti \
-e TZ=Asia/Shanghai --restart always \
--link rvc:rvc \
-d --name zhclean-web \
-p 4005:8080 bluerain/zhclean.rb