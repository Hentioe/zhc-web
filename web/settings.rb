require 'sinatra'

$environment = ARGV.length > 0 ? ARGV[0].to_sym : :development


set :port => 8080
set :public_folder, settings.root + '/web/static'
set :bind => '0.0.0.0'
set :cookie_options => {:httponly => false}
set :environment => $environment

#= 404 页面
not_found do
  status 404
  'Not found this page!'
end

#= 拦截器 禁止IE访问
before '/*' do
  if request.user_agent =~ /(MSIE \d)|(Trident\/7\.0)/
    redirect '/ie.html'
  end
end