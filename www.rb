#!/usr/bin/env ruby

require 'sinatra'
require 'sinatra/cookies'
require 'rest-client'
require 'isbot'
require_relative 'web/settings'

$resource_hash = nil

# 获取最新的资源 hash
def get_hash
  if production?
    url = 'http://rvc/core/zhclean/version'
    resp = RestClient.get(url)
    json = JSON.parse(resp.body)
    $resource_hash = json['data']
  elsif development?
    $resource_hash = File.read('./hash').to_s
  end
end

get_hash

#= ReactRouter
get /(\/?)|(\/explore)|(\/topic)/ do
  haml :index, locals: {
      :hash => $resource_hash
  }
end

#= Hook hash
get '/hook' do
  get_hash
end