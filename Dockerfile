FROM bluerain/ruby-2.5-slim:build-essential

ARG HOME_PATH=/usr/local/zhclean.rb


COPY ./ $HOME_PATH


WORKDIR $HOME_PATH


RUN gem install bundler && bundle


EXPOSE 8080


CMD ["ruby", "www.rb", "production"]